#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    setlocale(LC_ALL, "rus");
    MainWindow w;
    w.setWindowTitle("Antiplagiat");
    w.show();
    return a.exec();
}
