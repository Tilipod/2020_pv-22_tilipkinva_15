#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "plagiatcontrol.h"
#include <iostream>
#include <QTextCodec>
using namespace std;

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
  {
    ui->setupUi(this);
  }

MainWindow::~MainWindow()
  {
    delete ui;
  }


void MainWindow::on_goButton_clicked() 
  {
    SmartPtr<string> text (new string), fSample (new string);
    SmartPtr<std::map<std::string, unsigned>> res (new std::map<std::string, unsigned>);
    (*text) = (ui->textEdit->toPlainText()).toLocal8Bit().toStdString(); // read text
    (*fSample) = (ui->sampleEdit->toPlainText()).toStdString(); // read the way to file with the sample
    ui->plagiatEdit->clear(); // delete earlier result

    PlagiatControl plag(*fSample);
    if(!plag.plagiat(*text, *res))
        ui->plagiatEdit->setText(QString::fromLocal8Bit("������� �� ���������"));
    else
      {
        for (auto w : *res)
          {
            if (w.second)
              {
                QString buf = QString::fromLocal8Bit((w.first + " - " + to_string(w.second)).c_str());
                if (w.second == 1)
                    buf += QString::fromLocal8Bit(" ����������");
                else if (w.second >= 2 && w.second <= 4)
                    buf += QString::fromLocal8Bit(" ����������");
                else if (w.second >= 5)
                    buf += QString::fromLocal8Bit(" ����������");
                ui->plagiatEdit->append(buf);
              }
          }
      }
    
  }
