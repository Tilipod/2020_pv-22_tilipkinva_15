#include "plagiatcontrol.h"
#include <fstream>
#include <codecvt>
using namespace std;

// class StrUtility

// split - splits a string "line" by separators in an array "delArr" into a row vector

vector<string> WordWork::split(const string & line, const string& delArr)
  {
    vector<string> temp;

    unsigned iL = 0, iR = 0; // iL - left pos of word, iR - right pos of word
    while (iR < line.size())
      {
        if (delArr.find(line[iR]) != string::npos) // if delArr has char-pos iR, then char is del
          {
            temp.push_back(line.substr(iL, iR - iL)); // push found word
            while (iR < line.size() && delArr.find(line[iR]) != string::npos) // skip the next char-del
                iR++;
            iL = iR; // new left pos
          }
        iR++;
      }
    if (iL != line.size()) // if char-del is missing in the end line
        temp.push_back(line.substr(iL, line.size() - iL));

    return temp;
  }

// goToLower - translates Russian words "word" to lower case

string WordWork::goToLower(const string & word)
  {
    string temp = "", bufRUS = "�����Ũ��������������������������", bufrus = "��������������������������������";
    unsigned i = 0;
    while (i < word.size())
      {
        unsigned pos = bufRUS.find(word[i]);
        if (pos != string::npos)
            temp += bufrus[pos];
        else
            temp += word[i];
        i++;
      }
    return temp;
  }

// normAdjective - brings the Russian adjective "word" to the initial form

string WordWork::normAdjective (const string & word)
  {
    string temp = word.substr(word.size() - 2, 2);

    if (temp == "��" || temp == "��" || temp == "��" || temp == "��" || temp == "��" || temp == "��" || temp == "��" || temp == "��")
        return word.substr(0, word.size() - 2) + "��";

    temp = word.substr(word.size() - 3, 3);
    if (temp == "���" || temp == "���" || temp == "���")
        return word.substr(0, word.size() - 3) + "��";

    return word;
  }

// deleteEnding - removes the ending of a Russian verb or noun "word"

string WordWork::deleteEnding(const string & word)
  {
    string temp = word.substr(word.size() - 3, 3);

    if (temp == "���" || temp == "���" || temp == "���" || temp == "���" || temp == "���" || temp == "���" || temp == "���")
        return word.substr(0, word.size() - 3);

    temp = word.substr(word.size() - 2, 2);
    if (temp == "��" || temp == "��" || temp == "��" || temp == "��" || temp == "��" || temp == "��" || temp == "��" || temp == "��" || temp == "��" || temp == "��")
        return word.substr(0, word.size() - 2);

    temp = word.substr(word.size() - 1, 1);
    if (temp == "�" || temp == "�" || temp == "�" || temp == "�" || temp == "�" || temp == "�" || temp == "�")
        return word.substr(0, word.size() - 1);

    return word;
  }

// class "PlagiatControl, Section "public"

// setSample - set of the field "sample" from the file "fname"

void PlagiatControl::setSample(const string & fname)
  {
    ifstream fp(fname);
    string buf;

    sample.clear();
    while (fp)
      {
        getline(fp, buf);
        if (fp && buf != "")
            sample.push_back(buf);
      }
    fp.close();
    return;
  }

/* plagiat - Returns true if plagiarism of the sample is detected in the text "text", otherwise false.
   Counts the number of plagiarized words from the sample */

bool PlagiatControl::plagiat(const string & text, map<string, unsigned>& res)
  {
    if (!sample.size()) // void sample
        throw PlagiatVoidSample();

    bool ok = false;
    vector<string> textSplit = WordWork::split(text, " .,!?:;-\'\""), temp(sample);
    preparedMorphologic(textSplit);
    preparedMorphologic(temp);
    res.clear();

    for (unsigned i = 0; i < temp.size(); i++)
      {
        res[sample[i]] = 0;
        for (unsigned j = 0; j < textSplit.size(); j++)
          {
            if (temp[i].size() >= 3 && textSplit[j].find(temp[i]) == 0)
                res[sample[i]]++, ok = true;
          }
      }
    return ok;
  }

// Section "private"

// prepared - prepares vector "vec" words for morphological analysis

void PlagiatControl::preparedMorphologic(vector<string> & vec)
  {
    for (unsigned i = 0; i < vec.size(); i++) // prepared sample
      {
        if (vec[i].size() < 3)
            continue;
        vec[i] = WordWork::goToLower(vec[i]);
        vec[i] = WordWork::normAdjective(vec[i]);
        vec[i] = WordWork::deleteEnding(vec[i]);
      }
  }
