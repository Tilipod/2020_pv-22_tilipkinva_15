#ifndef PLAGIATCONTROL_H
#define PLAGIATCONTROL_H
#include <iostream>
#include <exception>
#include <vector>
#include <map>
using namespace std;

class PlagiatException : public exception
  {
    string message;

    public:

        PlagiatException() : message ("Error Plagiat Exception!") {}
        PlagiatException(const string mes) : message ("Error Plagiat Exception: " + mes) {}
        virtual ~PlagiatException() {}
        const char * what() const noexcept override {return message.c_str();}
  };

class PlagiatVoidSample : public PlagiatException // Error: the void sample
  {
    public:

        PlagiatVoidSample() : PlagiatException("Null-Ptr!") {}
        ~PlagiatVoidSample() {}
  };

template <class t_name>
class SmartPtr // class "Clever ptr"
  {
    t_name* ptrOb;

    public:

        SmartPtr() : ptrOb(NULL) {}
        SmartPtr(const SmartPtr& buf) : ptrOb(buf.ptrOb) {}
        SmartPtr(const t_name* ptr) : ptrOb(ptr) {}
        SmartPtr(t_name* ptr) : ptrOb(ptr) {}
        ~SmartPtr() {if (ptrOb != NULL) delete ptrOb;}

        SmartPtr& operator=(const SmartPtr& right)
          {
            this->ptrOb = right.ptrOb;
            return *this;
          }

        t_name* operator->() const {return ptrOb;}
        t_name& operator*() const {return *ptrOb;}

        t_name* getPtr() const {return ptrOb;}

  };

class WordWork // class "Useful utility for the wort to string/word"
  {
    public:

        static vector<string> split (const string&, const string&);
        static string goToLower(const string&);
        static string normAdjective (const string&);
        static string deleteEnding(const string&);

  };

class PlagiatControl // class "cotrol of the plagiat"
  {
    vector<string> sample;

    public:

        PlagiatControl() {}
        PlagiatControl(const string& fname) {setSample(fname);}

        vector<string> getSample() {return sample;}
        void setSample(const string&);
        bool plagiat(const string&, map<string, unsigned>&);

    private:

        void preparedMorphologic(vector<string>&);

  };

#endif // PLAGIATCONTROL_H
